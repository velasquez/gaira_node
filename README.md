# Gaira: private ethereum network

## INSTALL

1. install geth: https://github.com/ethereum/go-ethereum/wiki/Building-Ethereum
2. clone this repository

## RUN

1. go to this repository folder (cd gaira_node)
2. run ./start.sh (this is geth with the necesary arguments to connect to Gaira Network)
3. you can easily mine ether (> miner.start())

## Use Mist

1. download mist: https://github.com/ethereum/mist/releases
2. run it after executing ./start.sh