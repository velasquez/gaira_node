#!/usr/bin/env bash

geth --identity "GairaNode" --genesis custom_genesis.json --rpc --rpcport "8000" --rpccorsdomain "*" --datadir "chain" --port "30303" --nodiscover --ipcapi "admin,db,eth,debug,miner,net,shh,txpool,personal,web3" --rpcapi "db,eth,net,web3" --autodag --networkid 1900 --nat "upnp" --ipcpath "~/.ethereum/geth.ipc" console

